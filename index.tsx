import React from 'react';
import ReactDOM from 'react-dom';
import Main from './main'

(async function () {
    ReactDOM.render(<Main />, document.getElementById('root'))
}())