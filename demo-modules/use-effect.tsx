import React, { useState, useEffect } from 'react';

export default function UseEffect() {
    const [value, setValue] = useState(true);

    useEffect(() => {
        var elem = document.getElementById("test-dummy");
        elem.style.color = 'white';
        if (value) {
            elem.innerHTML = "loaded"
            elem.style.backgroundColor = 'red';
        } else {
            elem.innerHTML = "not loaded"
            elem.style.backgroundColor = 'green';
        }
    }, [value])

    return (
        <div>
            useEffect <br /><br />
            state value : {value.toString()} <br />
            <button onClick={() => { setValue(!value) }}>change value </button>
            <br /><br />
            effected component : <div id="test-dummy"></div>
        </div>
    )
}