import React, { useState } from 'react';
import useCovidTest, { covidFreeProps } from './use-covid-test';

export default function CustomHook() {
    const [someone, setSomeone]: any = useState({});

    const covidTestResult = useCovidTest(someone);

    return (
        <div>
            CustomHook <br /><br />

            <button onClick={() => {
                setSomeone({
                    caughing: true,
                    havingFever: true,
                    stayedAtHome: true,
                    wentOverSea: true,
                })
            }}>Ali</button> : <br />
            Caughing<br />
            Having a Fever<br />
            Stayed at Home<br />
            Recently Went Over sea<br />
            <br />

            <button onClick={() => {
                setSomeone({
                    visitedCovidPositivePerson: true,
                })
            }}>Abu</button> : <br />
            Recently Visited a Covid Positive Individual <br />
            <br />

            <button onClick={() => {
                setSomeone({
                    stayedAtHome: true,
                })
            }}>Ana</button> : <br />
            Stayed At Home <br />
            <br />

            covid test result : {covidTestResult}
        </div>
    )
}