import React, { useState, useEffect } from 'react';

export interface covidFreeProps {
    caughing?: boolean,
    havingFever?: boolean,
    wentOverSea?: boolean,
    visitedCovidPositivePerson?: boolean,
    stayedAtHome?: boolean,
}

export default function useCovidTest(props: covidFreeProps) {
    const [covidFree, setCovidFree] = useState('"please select an individual"');
    const {
        caughing = false,
        havingFever = false,
        wentOverSea = false,
        visitedCovidPositivePerson = false,
        stayedAtHome = false,
    } = props;

    useEffect(() => {
        if (stayedAtHome) {
            setCovidFree("low chance of having covid19")
        }
        if (wentOverSea || visitedCovidPositivePerson) {
            setCovidFree("need to be quarantined")
        }
        if (caughing && havingFever) {
            setCovidFree("high chance of having covid19!");
        }
    }, [props])

    return (
        covidFree
    )
}