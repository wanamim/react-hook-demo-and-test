import React, { useState } from 'react';

export default function UseState() {
    const [value, setValue] = useState(true);

    return (
        <div>
            useState <br /><br />
            state value : {value.toString()} <br />
            <button onClick={() => { setValue(!value) }}>change value </button>
        </div>
    )
}