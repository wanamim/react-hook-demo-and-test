import React, { useContext } from 'react';
import { MyContext } from './context';

export default function TestContextThree() {
    const { contextStates, poke }: any = useContext(MyContext);
    const { pokeFrom } = contextStates;

    return (
        <div>
            component THREE  &nbsp;
            <button onClick={() => { poke("three") }}>poke all</button>
            {pokeFrom !== "three" && pokeFrom !== null && <div>
                poked by component {pokeFrom}
            </div>}
        </div>
    )
}