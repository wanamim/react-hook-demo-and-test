import React, { createContext, useState } from 'react';

export const MyContext = createContext({});

export default function ContextInterface(props) {
    const [contextStates, setContextStates] = useState({ pokeFrom: null });

    function poke(from) {
        setContextStates({
            pokeFrom: from
        })
    }

    return (
        <MyContext.Provider value={{
            contextStates,
            poke,
        }}>
            {props.children}
        </MyContext.Provider>
    )
}