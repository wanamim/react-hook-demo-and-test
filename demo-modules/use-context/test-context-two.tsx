import React, { useContext } from 'react';
import { MyContext } from './context';

export default function TestContextTwo() {
    const { contextStates, poke }: any = useContext(MyContext);
    const { pokeFrom } = contextStates;

    return (
        <div>
            component TWO  &nbsp;
            <button onClick={() => { poke("two") }}>poke all</button>
            {pokeFrom !== "two" && pokeFrom !== null && <div>
                poked by component {pokeFrom}
            </div>}
        </div>
    )
}