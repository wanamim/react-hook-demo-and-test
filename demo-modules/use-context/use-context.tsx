import React, { useContext } from 'react';
import ContextInterface from './context';
import TestContextOne from './test-context-one';
import TestContextTwo from './test-context-two';
import TestContextThree from './test-context-three';

export default function UseContext() {
  return (
    <ContextInterface>
      <div>
        useContext <br /><br />
        <TestContextOne /> <br />
        <TestContextTwo /> <br />
        <TestContextThree /> <br />
      </div>
    </ContextInterface>
  )
}