import React, { useContext } from 'react';
import { MyContext } from './context';

export default function TestContextOne() {
    const { contextStates, poke }: any = useContext(MyContext);
    const { pokeFrom } = contextStates;

    return (
        <div>
            component ONE  &nbsp;
            <button onClick={() => { poke("one") }}>poke all</button>
            {pokeFrom !== "one" && pokeFrom !== null && <div>
                poked by component {pokeFrom}
            </div>}
        </div>
    )
}