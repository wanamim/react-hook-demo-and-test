import React, { useState, useEffect } from 'react';
import UseState from './demo-modules/use-state';
import UseEffect from './demo-modules/use-effect';
import UseContext from './demo-modules/use-context/use-context';
import CustomHook from './demo-modules/custom-hook/custom-hook';
import UseReducer from './demo-modules/use-reducer';

export default function Main() {
	const [curTut, setCurTut] = useState(1);

	function handleChangeModule(moduleNum: number) {
		localStorage.setItem("curTut", moduleNum.toString());
		setCurTut(moduleNum)
	}

	useEffect(() => {
		if (localStorage.getItem("curTut") !== null) {
			setCurTut(parseInt(localStorage.getItem("curTut")));
		}
	}, [])

	return (
		<div>
			React Hook Demo! &nbsp;
			<button onClick={() => { handleChangeModule(1) }}>useState</button>
			<button onClick={() => { handleChangeModule(2) }}>useEffect</button>
			<button onClick={() => { handleChangeModule(3) }}>useContext</button>
			<button onClick={() => { handleChangeModule(4) }}>customHook</button>
			<button onClick={() => { handleChangeModule(5) }}>useReducer</button>
			<br />
			<br />
			<div style={{ display: curTut === 1 ? "block" : "none" }}>
				<UseState />
			</div>
			<div style={{ display: curTut === 2 ? "block" : "none" }}>
				<UseEffect />
			</div>
			<div style={{ display: curTut === 3 ? "block" : "none" }}>
				<UseContext />
			</div>
			<div style={{ display: curTut === 4 ? "block" : "none" }}>
				<CustomHook />
			</div>
			<div style={{ display: curTut === 5 ? "block" : "none" }}>
				<UseReducer />
			</div>
		</div>
	)
}